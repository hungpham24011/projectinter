import { MyProject123TemplatePage } from './app.po';

describe('MyProject123 App', function() {
  let page: MyProject123TemplatePage;

  beforeEach(() => {
    page = new MyProject123TemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
